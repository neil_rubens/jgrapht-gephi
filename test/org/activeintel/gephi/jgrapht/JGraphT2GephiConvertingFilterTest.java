/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.jgrapht;

import java.util.Iterator;
import java.util.Set;
import org.gephi.graph.api.GraphView;
import org.gephi.filters.api.Query;
import org.openide.util.Lookup;
import org.gephi.filters.api.FilterController;
import junit.framework.Assert;
import java.io.FileNotFoundException;
import org.gephi.graph.api.GraphModel;
import org.activeintel.gephi.utilities.GephiUtilities;
import java.io.File;
import java.net.URISyntaxException;
import org.gephi.filters.spi.FilterProperty;
import org.gephi.graph.api.Graph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author neil
 */
public class JGraphT2GephiConvertingFilterTest {
    
    public JGraphT2GephiConvertingFilterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of filter method, of class JGraphT2GephiConvertingFilter.
     */
    @Test
    public void testFilter() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getResource("/org/activeintel/gephi/jgrapht/les-miserables.gexf").toURI());        
        GraphModel graphModel = GephiUtilities.importFile(file); 
        
        org.gephi.graph.api.Graph gGraph = graphModel.getGraph(); // Gephi's Graph
        org.jgrapht.Graph jGraph = JGraphTUtils.wrap(graphModel); // GraphT's Graph
        
        // Create & Execute Filter (this one is supposed to keep all of the nodes)
        JGraphT2GephiConvertingFilter filter = new JGraphT2GephiConvertingFilter(jGraph);
        FilterController controller = Lookup.getDefault().lookup(FilterController.class);
        Query query = controller.createQuery(filter);
        GraphView view = controller.filter(query); 
        // get the resulting subGraph
        Graph subGraph = graphModel.getGraph(view);
        
        // make sure that subGraph and graph are the same
        Assert.assertEquals( gGraph.getNodeCount(), subGraph.getNodeCount() );
        Assert.assertEquals( gGraph.getEdgeCount(), subGraph.getEdgeCount());  
        
        
        // remove a couple of edges & nodes from jGraph; chnages should be reflected in the filtered subgraph
        // test: remove 2 nodes
        int delNum = 2;
        Object[] nodes = jGraph.vertexSet().toArray();
        for(int i = 0; i < delNum; i++){
            jGraph.removeVertex(nodes[i]);            
        }
        // reinit filter
        filter = new JGraphT2GephiConvertingFilter(jGraph);        
        query = controller.createQuery(filter);
        view = controller.filter(query); 
        // get the resulting subGraph
        subGraph = graphModel.getGraph(view);
        // assert        
        Assert.assertEquals( gGraph.getNodeCount() - delNum, subGraph.getNodeCount() );        

        // test: remove 2 edges
        gGraph = subGraph; // update gGraph
        delNum = 2;
        Object[] edges = jGraph.edgeSet().toArray();
        for(int i = 0; i < delNum; i++){
            jGraph.removeEdge(edges[i]);            
        }
        // reinit filter
        filter = new JGraphT2GephiConvertingFilter(jGraph);        
        query = controller.createQuery(filter);
        view = controller.filter(query); 
        // get the resulting subGraph
        subGraph = graphModel.getGraph(view);
        // assert        
        Assert.assertEquals( gGraph.getEdgeCount() - delNum, subGraph.getEdgeCount() );                                                
    }


}
