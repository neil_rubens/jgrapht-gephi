/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.jgrapht;


import java.io.FileNotFoundException;
import java.io.File;
import java.net.URISyntaxException;
import org.gephi.graph.api.GraphModel;
import org.jgrapht.Graph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.core.IsEqual.*;
import static org.hamcrest.core.IsNot.*;
import junit.framework.Assert;
import org.activeintel.gephi.utilities.GephiUtilities;

/**
 *
 * @author neil
 */
public class JGraphTUtilsTest {
    
    public JGraphTUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of wrap method, of class JGraphTUtils.
     */
    @Test
    public void testWrapGraphModel() throws URISyntaxException, FileNotFoundException {
        //File file = new File(getClass().getResource("/org/activeintel/gephi/utilities/les-miserables.gexf").toURI());
        File file = new File(getClass().getResource("/org/activeintel/gephi/jgrapht/les-miserables.gexf").toURI());        
        GraphModel graphModel = GephiUtilities.importFile(file); 
        
        org.gephi.graph.api.Graph gGraph = graphModel.getGraph(); // Gephi's Graph
        Graph jGraph = JGraphTUtils.wrap(graphModel); // GraphT's Graph
        
        // Make sure that both graphs (Gephi's & JGraphT's) are equal:
        Assert.assertEquals( jGraph.vertexSet().size() , gGraph.getNodeCount());
        Assert.assertEquals( jGraph.edgeSet().size() , gGraph.getEdgeCount());  
        
        // Make sure that changes are not propagated w/in JGraphT graphs (changing g1 should not change g2)
        Graph jGraph2 = JGraphTUtils.wrap(graphModel); // GraphT's Graph
        jGraph2.removeEdge(jGraph2.edgeSet().iterator().next());
        jGraph2.removeVertex(jGraph2.vertexSet().iterator().next());
        assertThat( jGraph2.vertexSet().size(), not(equalTo(gGraph.getNodeCount())));        
        assertThat( jGraph2.edgeSet().size(),  not(equalTo(gGraph.getEdgeCount())));        
                
    }
    

    /**
     * Test of wrap method, of class JGraphTUtils.
     */
    @Test
    public void testWrapGraph() throws URISyntaxException, FileNotFoundException {
        //File file = new File(getClass().getResource("/org/activeintel/gephi/utilities/les-miserables.gexf").toURI());
        File file = new File(getClass().getResource("/org/activeintel/gephi/jgrapht/les-miserables.gexf").toURI());        
        GraphModel graphModel = GephiUtilities.importFile(file); 
        
        org.gephi.graph.api.Graph gGraph = graphModel.getGraph(); // Gephi's Graph
        Graph jGraph = JGraphTUtils.wrap(graphModel); // GraphT's Graph
        
        // get subGraph
        org.gephi.graph.api.Graph gSubGraph = graphModel.getGraph(graphModel.newView());
        // remove several nodes
        gSubGraph.removeNode(gSubGraph.getNodes().toArray()[0]);
        gSubGraph.removeNode(gSubGraph.getNodes().toArray()[0]);        
        
        // make sure wrapping does not throw exceptions
        JGraphTUtils.wrap(gSubGraph);
        
/*        
        // Make sure that both graphs (Gephi's & JGraphT's) are equal:
        Assert.assertEquals( jGraph.vertexSet().size() , gGraph.getNodeCount());
        Assert.assertEquals( jGraph.edgeSet().size() , gGraph.getEdgeCount());  
        
        // Make sure that changes are not propagated w/in JGraphT graphs (changing g1 should not change g2)
        Graph jGraph2 = JGraphTUtils.wrap(graphModel); // GraphT's Graph
        jGraph2.removeEdge(jGraph2.edgeSet().iterator().next());
        jGraph2.removeVertex(jGraph2.vertexSet().iterator().next());
        assertThat( jGraph2.vertexSet().size(), not(equalTo(gGraph.getNodeCount())));        
        assertThat( jGraph2.edgeSet().size(),  not(equalTo(gGraph.getEdgeCount())));        
*/                
    }
    
    
    
    /**
     * Test of wrap method, of class JGraphTUtils.
     */
    @Test
    public void testWrap_Load() throws URISyntaxException, FileNotFoundException {
        //File file = new File(getClass().getResource("/org/activeintel/gephi/utilities/les-miserables.gexf").toURI());
        File file = new File(getClass().getResource("/org/activeintel/gephi/jgrapht/les-miserables.gexf").toURI());        
        GraphModel graphModel = GephiUtilities.importFile(file); 
        
        for (int i = 0; i < 10000; i++){
            org.gephi.graph.api.Graph gGraph = graphModel.getGraph(); // Gephi's Graph
            Graph jGraph = JGraphTUtils.wrap(graphModel); // GraphT's Graph        
        }
    }
    
    
}
