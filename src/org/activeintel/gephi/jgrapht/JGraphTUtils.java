/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.jgrapht;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;

/**
 * Utilities for combining JGraphT w/ Gephi
 * 
 * Note: changes (e.g. removing node) to the org.gephi.graph.api.GraphModel
 * may affect org.jgrapht.Graph
 * 
 * @author Neil Rubens
 */
public class JGraphTUtils {
    
    
    private static final Logger log = Logger.getLogger(JGraphTUtils.class.getName());    
    
    /**
     * Wraps gephi's graphModel as
     * JGraphT's Graph
     * 
     * 
     * @param graphModel (of Gephi)
     * @return graph (of JGrapT)
     */
    public static Graph wrap(GraphModel graphModel){
        org.gephi.graph.api.Graph gGraph = graphModel.getGraph(); // Gephi Graph
                
        return wrap(gGraph);
    }
    

    /**
     * Wraps gephi's graph as
     * JGraphT's Graph
     * 
     * 
     * @param graph (of Gephi)
     * @return graph (of JGrapT)
     */    
    public static Graph wrap(org.gephi.graph.api.Graph gGraph){
        log.setLevel(Level.ALL);
        
        Graph<Node, Edge> jGraph = new DefaultDirectedWeightedGraph<Node, Edge>(Edge.class); //JGraphT Graph
        
        // Copy references from Gephi's Graph to JGrapT's graph
        // Nodes
        Node[] nodes = gGraph.getNodes().toArray();
        for(Node node: nodes){
            jGraph.addVertex(node);
            log.fine("node: " + node);
        }
        
        log.fine("nodes: " + jGraph.vertexSet());        
        
        // Edges
        Edge[] edges = gGraph.getEdges().toArray();
        for (Edge edge: edges){
  

            // TODO get the node from jGraph
            Node source = getNode(jGraph, edge.getSource());
            Node target = getNode(jGraph, edge.getTarget());
            
/*            
            Node source = edge.getSource();
            Node target = edge.getTarget();
*/            
            
            log.fine("edge: " + source + ":" + target);                    
            float weight = edge.getWeight();
            jGraph.addEdge(source, target, edge);
        }
        
        
        return jGraph;
        
    }    
    
    
    /**
     * 
     * @param jGraph
     * @param gNode (gephi's)
     * @return jNode that corresponds to node in the jGraph
     */
    public static Node getNode(Graph jGraph, Node gNode){
        for (Object o: jGraph.vertexSet()){
            Node n = (Node) o;
            if (n.getId() == gNode.getId()){
                return n;
            }
        }
        return null;
    }
    
}
