/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.jgrapht;

import java.util.HashSet;
import java.util.Set;
import org.gephi.filters.spi.ComplexFilter;
import org.gephi.filters.spi.FilterProperty;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.EdgeIterator;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.NodeIterable;
import org.gephi.graph.api.NodeIterator;
//import org.jgrapht.Graph;

/**
 * Returns equalent of JGraphT's graph
 * by filtering the main view of Gephi.
 * 
 * @see filter 
 * 
 * @author Neil Rubens
 */
public class JGraphT2GephiConvertingFilter implements ComplexFilter {
    /**
     * JGraphT graph; is used finding 'equivalent' graph in Gephi's view
     */
    private org.jgrapht.Graph jGraph;
    
    
    /**
     * Initializes the class
     * 
     * @param jGraph will be used for finding 'equivalent' graph in Gephi's view
     */
    public JGraphT2GephiConvertingFilter(org.jgrapht.Graph jGraph){
        this.jGraph = jGraph;
    }

    
    /**
     * Keeps only the nodes & edges in graph that are also in jGraph
     * 
     * @param graph
     * @return 
     */
    @Override
    public Graph filter(Graph graph) {
        // Filter out Nodes
        Set<Integer> jNodesIds = getNodesIds(jGraph.vertexSet().toArray());
        Node[] gNodes = graph.getNodes().toArray();
        for (Node gNode : gNodes){
            //System.out.println("gnodeID: " + gNode.getId() );
            if (!jNodesIds.contains(gNode.getId())){
                graph.removeNode(gNode);
            }
        }
        
        // Filter out Edges
        Set jEdgesIds = getEdgesIds(jGraph.edgeSet().toArray()); 
        Edge[] gEdges = graph.getEdges().toArray();
        for (Edge gEdge: gEdges){
            if (!jEdgesIds.contains(gEdge.getId())){
                graph.removeEdge(gEdge);
            }
        }

        return graph;
    }
    
    private Set<Integer> getNodesIds(Object[] nodes) {
        HashSet<Integer> ids = new HashSet<Integer>();
                
        for ( int i = 0; i < nodes.length; i++ ){
            Node node = (Node) nodes[i];
            ids.add(node.getId());
        }
        
        //System.out.println("node ids: " + ids );

        return ids;
    }
    

    private Set<Integer> getEdgesIds(Object[] edges) {
        HashSet<Integer> ids = new HashSet<Integer>();
                
        for ( int i = 0; i < edges.length; i++ ){
            Edge edge = (Edge) edges[i];
            ids.add(edge.getId());
        }

        return ids;
    }
    
    

    @Override
    public String getName() {
        return "JGraphT2GephiConvertingFilter";
    }

    @Override
    public FilterProperty[] getProperties() {
        return null;
    }


    
    
}
